package ru.webanimal.test43_single_activity_changingMenuOnPopBackStack.common;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.lang.ref.WeakReference;

public class BaseFragment extends Fragment {

    // =============================================================================================
    // Fields
    // =============================================================================================

    private WeakReference<BaseFeatureListener> listenerRef;


    // =============================================================================================
    // Public methods
    // =============================================================================================

    @Nullable
    public Context getAppContext() {
        Context ctx = getContext();

        if (ctx != null) {
            return ctx.getApplicationContext();
        }

        return null;
    }

    public boolean isActive() {
        return isResumed() && hasActivity();
    }

    public boolean hasActivity() {
        return getActivity() != null;
    }

    public boolean hasContext() {
        return getContext() != null;
    }

    public boolean hasListener() {
        return listenerRef.get() != null;
    }

    public void setListener(@Nullable final BaseFeatureListener listener) {
        listenerRef = new WeakReference<>(listener) ;
    }

    @Nullable
    public BaseFeatureListener getListener() {
        return listenerRef.get();
    }

    public long getTimestamp() {
        return System.currentTimeMillis();
    }
}
