package ru.webanimal.test43_single_activity_changingMenuOnPopBackStack.feature_list;

import ru.webanimal.test43_single_activity_changingMenuOnPopBackStack.common.BaseView;

public interface SomeListView extends BaseView {

    void onSomePresenterCommand();
}
