package ru.webanimal.test43_single_activity_changingMenuOnPopBackStack.feature_list;

import androidx.annotation.NonNull;

public class SomeListItemModelUI {

    // =============================================================================================
    // Fields
    // =============================================================================================

    private String title;
    private String text;
    private String description;


    // =============================================================================================
    // Constructor
    // =============================================================================================

    public SomeListItemModelUI(
            @NonNull final String title,
            @NonNull final String text,
            @NonNull final String description) {

        this.title = title;
        this.text = text;
        this.description = description;
    }


    // =============================================================================================
    // Getters and Setters
    // =============================================================================================

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    // =============================================================================================
    // Public methods
    // =============================================================================================

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SomeListItemModelUI that = (SomeListItemModelUI) o;

        if (!title.equals(that.title)) return false;
        if (!text.equals(that.text)) return false;
        return description.equals(that.description);
    }

    @Override
    public int hashCode() {
        int result = title.hashCode();
        result = 31 * result + text.hashCode();
        result = 31 * result + description.hashCode();
        return result;
    }
}
