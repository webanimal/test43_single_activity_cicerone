package ru.webanimal.test43_single_activity_changingMenuOnPopBackStack.feature_list;

import ru.webanimal.test43_single_activity_changingMenuOnPopBackStack.common.BaseFeatureListener;

public interface SomeListFeatureListener extends BaseFeatureListener {

    void fromSomeListToHostActivity();
}
