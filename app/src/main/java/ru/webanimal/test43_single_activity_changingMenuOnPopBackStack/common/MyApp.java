package ru.webanimal.test43_single_activity_changingMenuOnPopBackStack.common;

import androidx.annotation.NonNull;
import androidx.multidex.MultiDexApplication;

import ru.terrakok.cicerone.Cicerone;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;

public class MyApp extends MultiDexApplication {

    // =============================================================================================
    // Static
    // =============================================================================================

    public static MyApp INSTANCE;


    // =============================================================================================
    // Fields
    // =============================================================================================

    private Cicerone<Router> cicerone;


    // =============================================================================================
    // Application callbacks
    // =============================================================================================

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
        initCicerone();
    }


    // =============================================================================================
    // Public methods
    // =============================================================================================

    @NonNull
    public NavigatorHolder getNavigatorHolder() {
        return cicerone.getNavigatorHolder();
    }

    @NonNull
    public Router getRouter() {
        return cicerone.getRouter();
    }


    // =============================================================================================
    // Private methods
    // =============================================================================================

    private void initCicerone() {
        cicerone = Cicerone.create();
    }
}
