package ru.webanimal.test43_single_activity_changingMenuOnPopBackStack.feature_list;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import ru.webanimal.test43_single_activity_changingMenuOnPopBackStack.R;

public class SomeListItemViewHolder extends RecyclerView.ViewHolder {

    // =============================================================================================
    // Static methods
    // =============================================================================================

    public static final SomeListItemViewHolder create(@NonNull final View itemView) {
        return new SomeListItemViewHolder(itemView);
    }


    // =============================================================================================
    // RecyclerView.ViewHolder callbacks
    // =============================================================================================

    public SomeListItemViewHolder(@NonNull final View itemView) {
        super(itemView);
    }


    // =============================================================================================
    // Public methods
    // =============================================================================================

    public void bindItem(@NonNull final View itemView, @Nullable final SomeListItemModelUI item) {
        if (item == null) {
            return;
        }

        ((AppCompatTextView) itemView.findViewById(R.id.some_list_item_title_tv))
                .setText(item.getTitle());

        ((AppCompatTextView) itemView.findViewById(R.id.some_list_item_text_tv))
                .setText(item.getText());

        ((AppCompatTextView) itemView.findViewById(R.id.some_list_item_description_tv))
                .setText(item.getDescription());
    }
}
