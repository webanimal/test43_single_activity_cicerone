package ru.webanimal.test43_single_activity_changingMenuOnPopBackStack.common;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleObserver;

import ru.terrakok.cicerone.Router;

public class BasePresenter implements LifecycleObserver {

    // =============================================================================================
    // Fields
    // =============================================================================================

    private BaseView viewImpl;


    // =============================================================================================
    // Constructor
    // =============================================================================================

    public BasePresenter(@NonNull BaseView viewImpl) {
        this.viewImpl = viewImpl;
    }


    // =============================================================================================
    // Public methods
    // =============================================================================================

    public boolean hasView() {
        return viewImpl != null;
    }

    @NonNull
    public BaseView getView() {
        return viewImpl;
    }

    @NonNull
    public Router getRouter() {
        return MyApp.INSTANCE.getRouter();
    }
}
