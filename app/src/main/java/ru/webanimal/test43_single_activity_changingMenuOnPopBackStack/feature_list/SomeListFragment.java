package ru.webanimal.test43_single_activity_changingMenuOnPopBackStack.feature_list;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import ru.webanimal.test43_single_activity_changingMenuOnPopBackStack.R;
import ru.webanimal.test43_single_activity_changingMenuOnPopBackStack.common.BaseFragment;

public class SomeListFragment extends BaseFragment implements SomeListView {

    // =============================================================================================
    // Static fields
    // =============================================================================================

    public static SomeListFragment newInstance() {
        return new SomeListFragment();
    }


    // =============================================================================================
    // Static fields
    // =============================================================================================

    private static final int LAYOUT = R.layout.fragment_some_list;


    // =============================================================================================
    // Fields
    // =============================================================================================

    private SomeListPresenter presenter;


    // =============================================================================================
    // Listeners
    // =============================================================================================

    private View.OnClickListener clickMeListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (presenter == null) {
                return;
            }

            presenter.onSomeViewAction();
        }
    };


    // =============================================================================================
    // Fragment callbacks
    // =============================================================================================


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        setupHostListener(true);
        setupLifecycle(true);
    }

    @Nullable
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(LAYOUT, container, false);
        setupUI(root);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();

        setupUX(true);
    }

    @Override
    public void onPause() {
        super.onPause();

        setupUX(false);
    }

    @Override
    public void onDetach() {
        super.onDetach();

        setupHostListener(false);
        setupLifecycle(false);
    }


    // =============================================================================================
    // BaseFragment callbacks
    // =============================================================================================

    @Nullable
    @Override
    public SomeListFeatureListener getListener() {
        return (SomeListFeatureListener) super.getListener();
    }


    // =============================================================================================
    // SomeListView callbacks
    // =============================================================================================

    @Override
    public void onSomePresenterCommand() {
        if (isActive()) {
            ((AppCompatTextView) getActivity().findViewById(R.id.some_list_title_tv)).setText(
                    getString(R.string.fragment_some_list_title_with_timestamp, getTimestamp())
            );
        }
    }


    // =============================================================================================
    // Private methods
    // =============================================================================================

    private void setupUI(@NonNull final View root) {
        // tmp
        ArrayList<SomeListItemModelUI> items = new ArrayList<>(5);
        items.add(new SomeListItemModelUI(getString(R.string.stub_person_full_name), getString(R.string.stub_person_birthday), getString(R.string.stub_person_address)));
        items.add(new SomeListItemModelUI(getString(R.string.stub_person_full_name), getString(R.string.stub_person_birthday), getString(R.string.stub_person_address)));
        items.add(new SomeListItemModelUI(getString(R.string.stub_person_full_name), getString(R.string.stub_person_birthday), getString(R.string.stub_person_address)));
        items.add(new SomeListItemModelUI(getString(R.string.stub_person_full_name), getString(R.string.stub_person_birthday), getString(R.string.stub_person_address)));
        items.add(new SomeListItemModelUI(getString(R.string.stub_person_full_name), getString(R.string.stub_person_birthday), getString(R.string.stub_person_address)));
        items.add(new SomeListItemModelUI(getString(R.string.stub_person_full_name), getString(R.string.stub_person_birthday), getString(R.string.stub_person_address)));
        items.add(new SomeListItemModelUI(getString(R.string.stub_person_full_name), getString(R.string.stub_person_birthday), getString(R.string.stub_person_address)));

        RecyclerView recycler = root.findViewById(R.id.some_list_recycler);
        recycler.setAdapter(new SomeListAdapter(items));
    }

    private void setupUX(final boolean setEnabled) {
        if (hasActivity()) {
            getActivity().findViewById(R.id.some_list_click_me_btn).setOnClickListener(clickMeListener);
        }
    }

    private void setupHostListener(boolean setEnabled) {
        setListener(setEnabled
                ? (hasActivity() && getActivity() instanceof SomeListFeatureListener)
                ? (SomeListFeatureListener) getActivity()
                : null
                : null
        );
    }

    private void setupLifecycle(boolean setEnabled) {
        if (setEnabled) {
            presenter = new SomeListPresenter(this);
            getLifecycle().addObserver(presenter);
            return;
        }

        getLifecycle().removeObserver(presenter);
        presenter = null;
    }
}
