package ru.webanimal.test43_single_activity_changingMenuOnPopBackStack.feature_list;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;

import ru.webanimal.test43_single_activity_changingMenuOnPopBackStack.R;

public class SomeListAdapter extends RecyclerView.Adapter<SomeListItemViewHolder> {

    // =============================================================================================
    // Fields
    // =============================================================================================

    private ArrayList<SomeListItemModelUI> items;


    // =============================================================================================
    // Constructor
    // =============================================================================================

    public SomeListAdapter() {
        this.items = new ArrayList<>();
    }

    public SomeListAdapter(@NonNull final ArrayList<SomeListItemModelUI> items) {
        this.items = new ArrayList<>(items.size());
        this.items.addAll(items);
    }


    // =============================================================================================
    // RecyclerView.Adapter<SomeListItemViewHolder> callbacks
    // =============================================================================================

    @NonNull
    @Override
    public SomeListItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return SomeListItemViewHolder.create(
                LayoutInflater
                        .from(parent.getContext())
                        .inflate(R.layout.adapter_item_some_list, parent, false)
        );
    }

    public void onBindViewHolder(@NonNull final SomeListItemViewHolder holder, int position) {
        holder.bindItem(holder.itemView, items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
