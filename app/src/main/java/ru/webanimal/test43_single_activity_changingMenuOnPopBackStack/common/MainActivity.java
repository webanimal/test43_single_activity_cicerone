package ru.webanimal.test43_single_activity_changingMenuOnPopBackStack.common;

import android.os.Bundle;

import ru.webanimal.test43_single_activity_changingMenuOnPopBackStack.R;
import ru.webanimal.test43_single_activity_changingMenuOnPopBackStack.feature_list.SomeListFeatureListener;

public class MainActivity extends BaseFragmentHostActivity implements SomeListFeatureListener {

    // =============================================================================================
    // Activity callbacks
    // =============================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onResume() {
        super.onResume();
        startInitialScreen();
    }


    // =============================================================================================
    // SomeListFeatureListener callbacks
    // =============================================================================================

    @Override
    public void fromSomeListToHostActivity() {
    }
}
