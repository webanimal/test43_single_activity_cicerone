package ru.webanimal.test43_single_activity_changingMenuOnPopBackStack.common;

import androidx.appcompat.app.AppCompatActivity;

import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;
import ru.terrakok.cicerone.android.support.SupportAppNavigator;
import ru.webanimal.test43_single_activity_changingMenuOnPopBackStack.R;

public class BaseFragmentHostActivity extends AppCompatActivity {

    // =============================================================================================
    // Fields
    // =============================================================================================

    private boolean isNavigatorRemoved = true;


    // =============================================================================================
    // Activity callbacks
    // =============================================================================================

    @Override
    protected void onResume() {
        super.onResume();
        setupNavigator(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        setupNavigator(false);
    }


    // =============================================================================================
    // Public methods
    // =============================================================================================

    /**
     * Can be launched only after a Navigator is set up.<br>
     * Navigator is setting up in a onResume() and removing in a onPause()<br>
     * Please start from a {@link MainActivity#onResume()} after super.onResume() for a better visual control.
     */
    public void startInitialScreen() throws RuntimeException {
        checkNavigationAccessibility();
        getRouter().replaceScreen(new Screens.SomeListScreen());
    }


    // =============================================================================================
    // Private methods
    // =============================================================================================

    private void setupNavigator(boolean setEnabled) {
        if (setEnabled) {
            getNavigatorHolder().setNavigator(
                    new SupportAppNavigator(this, R.id.lay_fragment_host_container)
            );
            this.isNavigatorRemoved = false;
            return;
        }

        getNavigatorHolder().removeNavigator();
        this.isNavigatorRemoved = true;
    }

    private void checkNavigationAccessibility() throws RuntimeException {
        if (isNavigatorRemoved) {
            throw new RuntimeException("Can't navigate before a SupportAppNavigator is set up");
        }
    }

    private NavigatorHolder getNavigatorHolder() {
        return MyApp.INSTANCE.getNavigatorHolder();
    }

    private Router getRouter() {
        return MyApp.INSTANCE.getRouter();
    }
}
