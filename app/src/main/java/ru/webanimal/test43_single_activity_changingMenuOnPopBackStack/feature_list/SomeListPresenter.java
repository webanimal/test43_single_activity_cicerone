package ru.webanimal.test43_single_activity_changingMenuOnPopBackStack.feature_list;

import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.OnLifecycleEvent;

import ru.webanimal.test43_single_activity_changingMenuOnPopBackStack.common.BasePresenter;

public class SomeListPresenter extends BasePresenter {

    // =============================================================================================
    // Fields
    // =============================================================================================

    private boolean isViewResumed;


    // =============================================================================================
    // Constructor
    // =============================================================================================

    public SomeListPresenter(@NonNull final SomeListView viewImpl) {
        super(viewImpl);
    }


    // =============================================================================================
    // BasePresenter callbacks
    // =============================================================================================

    @NonNull
    @Override
    public SomeListView getView() {
        return (SomeListView) super.getView();
    }


    // =============================================================================================
    // LifeCycleObservable callbacks
    // =============================================================================================

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    private void onResume() {
        isViewResumed = true;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    private void onPause() {
        isViewResumed = false;
    }


    // =============================================================================================
    // Public methods
    // =============================================================================================

    public void onSomeViewAction() {
        doSomeAsyncCommand();
    }

    public void navBack() {
        getRouter().exit();
    }


    // =============================================================================================
    // Private methods
    // =============================================================================================

    private void doSomeAsyncCommand() {
        new Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        if (isViewResumed && hasView()) {
                             getView().onSomePresenterCommand();
                        }
                    }
                },
                500L
        );
    }
}
