package ru.webanimal.test43_single_activity_changingMenuOnPopBackStack.common;

import androidx.fragment.app.Fragment;

import ru.terrakok.cicerone.android.support.SupportAppScreen;
import ru.webanimal.test43_single_activity_changingMenuOnPopBackStack.feature_list.SomeListFragment;

public class Screens {

    public static final class SomeListScreen extends SupportAppScreen {

        @Override
        public Fragment getFragment() {
            return SomeListFragment.newInstance();
        }
    }
}
